metropolitan_areas = [
    # (city, country, population, area)
    ('Tokyo', 'Japan', 37400068, 13452),
    ('Delhi', 'India', 28514000, 3483),
    ('Shanghai', 'China', 25582000, 6341),
    ('São Paulo', 'Brasil', 21650000, 7947),
    ('Mexico City', 'Mexico', 21581000, 7866),
    ('Mumbai', 'India', 24400000, 4355),
    ('Cairo', 'Egypt', 20076000, 9844),
    ('Bejing', 'China', 19618000, 1334)
]

cities = ['London', 'Manchester', 'Paris', 'Rome', 'Moscow']
countries = ['UK', 'UK', 'France', 'Italy', 'Russia']

