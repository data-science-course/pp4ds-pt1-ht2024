# PP4DS PT1 HT2024


## Python Programming for Data Science I <img src="oudce_logo.png" align="right"/>

### Massimiliano Izzo 

Materials for [Python Programming for Data Science F2F Course](https://gitlab.com/data-science-course/pp4ds-pt1-ht2024) - **this page will be updated as the course progresses**.

The class workspace on **Slack** is https://pp4ds-ox.slack.com. I encourage you to ask questions should you have them in the Slack channel incase your classmates can help. Massi (your tutor; massimiliano.izzo@conted.ox.ac.uk) will also check Slack and provide support where possible. Download Slack from: https://slack.com/get

To use **Jupyter** yourself, I recommend you download and install **Anaconda**, a Python Data Science Platform, from: [here](https://www.anaconda.com/products/individual) Make sure you download the **Python 3** version of Anaconda, ideally Python 3.9+. You can also install Jupyter if you have a standard Python distribution installed. Ask your tutors for assistance if you need to install Jupyter on your own machine.

To get the contents of this repository I recommend that you install **Git SCM**, a source code management software, that will help you keep up-to-date with the repository. I will be adding content as the course progresses and Git will allow you to pull new material as it becomes available.

You can also run online live versions of the notebooks that are launched by **[Binder](https://mybinder.org)** by clicking on the `binder` buttons below without having to install anything yourself. Please note that Binder is still in beta testing and is hosted by *University of California, Berkeley* so may occasionally not work as expected (but is quite reliable). 

### Cloning this repository to use it offline

If you want to run the notebooks on your own computer at home, apart from installing Jupyter/Anaconda as per above, you will need to install **Git**, which is a source code management software, from [here](https://git-scm.com/downloads). Windows users can also get Git here: https://gitforwindows.org/. Once installed, you need to open up the command-line ("Command Prompt" on Windows or "Terminal" on Mac OSX) to run some commands.

Change directory to somewhere sensible, such as `My Documents` or similar on Windows or `Documents` on Mac OSX. Assuming you're using `Documents`:

```bash
cd Documents
```

Then ask Git to clone this repository with the following command.
```bash
git clone https://gitlab.com/data-science-course/pp4ds-pt1-ht2024.git
```
or, if you have SSH access enabled
```bash
git clone git@gitlab.com:data-science-course/pp4ds-pt1-ht2024.git
```

This will create a subdirectory called `pp4ds-pt1-ht2024` in your `Documents` folder. When you need to update the content at some later time after I have added some new files to the repository, you will need to open up the command-line again and do the following commands.

```bash
cd Documents/pp4ds-pt1-ht2024
git pull
```
What this does is to ask Git to check if there are any new changes in the online repository and to download those new files or updates to the existing files.

Either some lines of stuff should whizz by, or it will say `Already up to date.` if there are no new changes.

If this doesn't work, you may need to force the update, which will overwrite your local files. To do this (make sure any of your own work is renamed or moved outside of the `pp4ds-pt1-ht2024` folder first):

```bash
git fetch --all
git reset --hard origin/master
```

### Course Programme

**Week 1:**  Introduction to Data Science 

**Week 2:**  Python basics: built-in types, functions and methods, if statement

**Week 3:**  Python data structures: list, dicts, tuples, sets; for loops 

**Week 4:**  NumPy and the SciPy ecosistem. Basic statistics with NumPy

**Week 5:**  Pandas for data science I

**Week 6:**  Pandas for data science II

**Week 7:**  Data visualisation: matplotlib and seaborn

**Week 8:**  Object-oriented programming: classes, inheritance, and applications

**Week 9:**  Data gathering and cleaning. Text pre-processing

**Week 10:**  Introduction to experimental design and statistical test. Time-series Analysis.


## Week 1: Introduction to Data Science

* Lecture notes- copy and paste link to new browser tab to download: http://tinyurl.com/mruxn27f 
* Exercise 01A: Notebook Basic [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F01a_Notebook_Basics.ipynb)
* Exercise 01B: Running Code [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F01b_Running_Code.ipynb)
* Exercise 01C: Working with Markdown [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F01c_Working_With_Markdown_Cells.ipynb)
* Exercise 01D: Notebook Exercises [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F01d_Notebook_Exercises.ipynb)

## Week 2: Python basics: built-in types, functions and methods, if statement

* Lecture notes - copy and paste link to new browser tab to download: http://tinyurl.com/yby4u68p
* Live Demo [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_02.ipynb)
* Exercise 02: Expressions and more [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises%2F02_Expressions.ipynb)
* Exercise 02: **solutions** [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises-solutions%2F02_Expressions_complete.ipynb)

## Week 3: Data structures in Python. lists, tuples and list comprehension

* Lecture notes - copy and paste link to new browser tab to download: http://tinyurl.com/2f7drsx6
* Live Demo [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_03.ipynb)
* Exercise 03: Data Structures and Loops - do exercises 3.1-3.3, 3.7, 3.9, 3.10 only: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises%2F03_Data_Structures_and_Loops.ipynb)
* Exercise 03 **solutions** - exercises 3.1-3.3, 3.7, 3.9, 3.10 only: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises-solutions%2F03a_Data_Structures_and_Loops.ipynb)

## Week 4: Dictionaries and sets. Python standard library. Intro to NumPy

* Lecture notes - copy and paste link to new browser tab to download: http://tinyurl.com/mstjrbav
* Live Demo - Dicts and sets [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_04a.ipynb)
* Live Demo - Intro to NumPy [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_04b.ipynb)
* Exercise 03: Data Structures and Loops - all the remaining exercises: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises%2F03_Data_Structures_and_Loops.ipynb)
* Exercise 04: Intro to NumPy [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F04_Numpy_exercises.ipynb)
* Exercise 03 **solutions** - full: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=exercises-solutions%2F03b_Data_Structures_and_Loops.ipynb)

## Week 5: More NumPy. Intro to statistics

* Lecture notes - copy and paste link to new browser tab to download: http://tinyurl.com/y3xyfsnc
* Live Demo - Intro to NumPy (complete) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_05a.ipynb)
* Live Demo - Statistics with NumPy [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_05b.ipynb)
* Exercise 05: NumPy exercises (fully)  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F05_Numpy_exercises.ipynb)
* Exercise 05: **solutions**  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises-solutions%2F05_Numpy_exercises.ipynb)

## Week 6: Pandas (I)

* Lecture notes - copy and paste link to new browser tab to download: http://tinyurl.com/ndwyk6xd
* Live Demo - Intro to Pandas [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_06.ipynb)
* First Assignment: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=assignments%2FFirst_Assignment.ipynb)


## Week 7: Data exploration and analysis with Pandas (II)

* Lecture notes - copy and paste link to new browser tab to download: https://tinyurl.com/4267rd46
* Live Demo - More Pandas [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_07.ipynb)

## Week 8: Split-apply-combine. ~~Intro to data visualisation~~

~~* Lecture notes - copy and paste link to new browser tab to download: https://tinyurl.com/42m84bwf~~
* Live Demo - More Pandas (cont'd)
* Exercise 08: Exploration of the Titanic Dataset  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises%2F08_The_Titanic_Dataset.ipynb)
* Exercise 08: **solutions**  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=exercises-solutions%2F08_The_Titanic_Dataset.ipynb)

## Week 9: Data visualisation

* Lecture notes - copy and paste link to new browser tab to download: https://tinyurl.com/4ww4zt5x
* Live Demo - Matplotlib exploration [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_08.ipynb)
* Second Assignment: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/HEAD?labpath=assignments%2FSecond_Assignment.ipynb)

## Week 10: Classes + data gathering

* Lecture notes - copy and paste link to new browser tab to download: https://tinyurl.com/2sfzczww
* Live Demo [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/data-science-course%2Fpp4ds-pt1-ht2024/main?labpath=live-demos%2FWeek_10.ipynb)
